<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Pizza
 */

?>

	</div>

	<footer id="colophon" class="site-footer">
		<div class="main-navigation">
			<?php pizza_footer_navigation_menu(); ?>
		</div>
		<div class="site-wrapper">


			<p>All right reserved &copy; 2016 - <?php echo date('Y'); ?> <?php echo bloginfo('name'); ?></p>
		
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
