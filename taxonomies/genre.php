<?php

/**
 * Registers the `genre` taxonomy,
 * for use with 'movie'.
 */
function genre_init() {
	register_taxonomy( 'genre', array( 'movie', 'post' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Genres', 'wpcourse' ),
			'singular_name'              => _x( 'Genre', 'taxonomy general name', 'wpcourse' ),
			'search_items'               => __( 'Search Genres', 'wpcourse' ),
			'popular_items'              => __( 'Popular Genres', 'wpcourse' ),
			'all_items'                  => __( 'All Genres', 'wpcourse' ),
			'parent_item'                => __( 'Parent Genre', 'wpcourse' ),
			'parent_item_colon'          => __( 'Parent Genre:', 'wpcourse' ),
			'edit_item'                  => __( 'Edit Genre', 'wpcourse' ),
			'update_item'                => __( 'Update Genre', 'wpcourse' ),
			'view_item'                  => __( 'View Genre', 'wpcourse' ),
			'add_new_item'               => __( 'Add New Genre', 'wpcourse' ),
			'new_item_name'              => __( 'New Genre', 'wpcourse' ),
			'separate_items_with_commas' => __( 'Separate genres with commas', 'wpcourse' ),
			'add_or_remove_items'        => __( 'Add or remove genres', 'wpcourse' ),
			'choose_from_most_used'      => __( 'Choose from the most used genres', 'wpcourse' ),
			'not_found'                  => __( 'No genres found.', 'wpcourse' ),
			'no_terms'                   => __( 'No genres', 'wpcourse' ),
			'menu_name'                  => __( 'Genres', 'wpcourse' ),
			'items_list_navigation'      => __( 'Genres list navigation', 'wpcourse' ),
			'items_list'                 => __( 'Genres list', 'wpcourse' ),
			'most_used'                  => _x( 'Most Used', 'genre', 'wpcourse' ),
			'back_to_items'              => __( '&larr; Back to Genres', 'wpcourse' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'genre',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'genre_init' );

/**
 * Sets the post updated messages for the `genre` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `genre` taxonomy.
 */
function genre_updated_messages( $messages ) {

	$messages['genre'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Genre added.', 'wpcourse' ),
		2 => __( 'Genre deleted.', 'wpcourse' ),
		3 => __( 'Genre updated.', 'wpcourse' ),
		4 => __( 'Genre not added.', 'wpcourse' ),
		5 => __( 'Genre not updated.', 'wpcourse' ),
		6 => __( 'Genres deleted.', 'wpcourse' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'genre_updated_messages' );
