<?php

/**
 * Registers the `year` taxonomy,
 * for use with 'movie'.
 */
function year_init() {
	register_taxonomy( 'year', array( 'movie', 'post' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Years', 'wpcourse' ),
			'singular_name'              => _x( 'Year', 'taxonomy general name', 'wpcourse' ),
			'search_items'               => __( 'Search Years', 'wpcourse' ),
			'popular_items'              => __( 'Popular Years', 'wpcourse' ),
			'all_items'                  => __( 'All Years', 'wpcourse' ),
			'parent_item'                => __( 'Parent Year', 'wpcourse' ),
			'parent_item_colon'          => __( 'Parent Year:', 'wpcourse' ),
			'edit_item'                  => __( 'Edit Year', 'wpcourse' ),
			'update_item'                => __( 'Update Year', 'wpcourse' ),
			'view_item'                  => __( 'View Year', 'wpcourse' ),
			'add_new_item'               => __( 'Add New Year', 'wpcourse' ),
			'new_item_name'              => __( 'New Year', 'wpcourse' ),
			'separate_items_with_commas' => __( 'Separate years with commas', 'wpcourse' ),
			'add_or_remove_items'        => __( 'Add or remove years', 'wpcourse' ),
			'choose_from_most_used'      => __( 'Choose from the most used years', 'wpcourse' ),
			'not_found'                  => __( 'No years found.', 'wpcourse' ),
			'no_terms'                   => __( 'No years', 'wpcourse' ),
			'menu_name'                  => __( 'Years', 'wpcourse' ),
			'items_list_navigation'      => __( 'Years list navigation', 'wpcourse' ),
			'items_list'                 => __( 'Years list', 'wpcourse' ),
			'most_used'                  => _x( 'Most Used', 'year', 'wpcourse' ),
			'back_to_items'              => __( '&larr; Back to Years', 'wpcourse' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'year',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'year_init' );

/**
 * Sets the post updated messages for the `year` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `year` taxonomy.
 */
function year_updated_messages( $messages ) {

	$messages['year'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Year added.', 'wpcourse' ),
		2 => __( 'Year deleted.', 'wpcourse' ),
		3 => __( 'Year updated.', 'wpcourse' ),
		4 => __( 'Year not added.', 'wpcourse' ),
		5 => __( 'Year not updated.', 'wpcourse' ),
		6 => __( 'Years deleted.', 'wpcourse' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'year_updated_messages' );
