<?php

/**
 * Registers the `movie` post type.
 */
function movie_init() {
	register_post_type( 'movie', array(
		'labels'                => array(
			'name'                  => __( 'Movies', 'wpcourse' ),
			'singular_name'         => __( 'Movie', 'wpcourse' ),
			'all_items'             => __( 'All Movies', 'wpcourse' ),
			'archives'              => __( 'Movie Archives', 'wpcourse' ),
			'attributes'            => __( 'Movie Attributes', 'wpcourse' ),
			'insert_into_item'      => __( 'Insert into movie', 'wpcourse' ),
			'uploaded_to_this_item' => __( 'Uploaded to this movie', 'wpcourse' ),
			'featured_image'        => _x( 'Featured Image', 'movie', 'wpcourse' ),
			'set_featured_image'    => _x( 'Set featured image', 'movie', 'wpcourse' ),
			'remove_featured_image' => _x( 'Remove featured image', 'movie', 'wpcourse' ),
			'use_featured_image'    => _x( 'Use as featured image', 'movie', 'wpcourse' ),
			'filter_items_list'     => __( 'Filter movies list', 'wpcourse' ),
			'items_list_navigation' => __( 'Movies list navigation', 'wpcourse' ),
			'items_list'            => __( 'Movies list', 'wpcourse' ),
			'new_item'              => __( 'New Movie', 'wpcourse' ),
			'add_new'               => __( 'Add New', 'wpcourse' ),
			'add_new_item'          => __( 'Add New Movie', 'wpcourse' ),
			'edit_item'             => __( 'Edit Movie', 'wpcourse' ),
			'view_item'             => __( 'View Movie', 'wpcourse' ),
			'view_items'            => __( 'View Movies', 'wpcourse' ),
			'search_items'          => __( 'Search movies', 'wpcourse' ),
			'not_found'             => __( 'No movies found', 'wpcourse' ),
			'not_found_in_trash'    => __( 'No movies found in trash', 'wpcourse' ),
			'parent_item_colon'     => __( 'Parent Movie:', 'wpcourse' ),
			'menu_name'             => __( 'Movies', 'wpcourse' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => 'movies',
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-video-alt',
		'show_in_rest'          => true,
		'rest_base'             => 'movie',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'movie_init' );

/**
 * Sets the post updated messages for the `movie` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `movie` post type.
 */
function movie_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['movie'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Movie updated. <a target="_blank" href="%s">View movie</a>', 'wpcourse' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'wpcourse' ),
		3  => __( 'Custom field deleted.', 'wpcourse' ),
		4  => __( 'Movie updated.', 'wpcourse' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Movie restored to revision from %s', 'wpcourse' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Movie published. <a href="%s">View movie</a>', 'wpcourse' ), esc_url( $permalink ) ),
		7  => __( 'Movie saved.', 'wpcourse' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Movie submitted. <a target="_blank" href="%s">Preview movie</a>', 'wpcourse' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Movie scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview movie</a>', 'wpcourse' ),
		date_i18n( __( 'M j, Y @ G:i', 'wpcourse' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Movie draft updated. <a target="_blank" href="%s">Preview movie</a>', 'wpcourse' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'movie_updated_messages' );
